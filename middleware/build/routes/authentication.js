"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
/** src/routes/applications.ts */
const express_1 = __importDefault(require("express"));
const authenticationRoutes_1 = __importDefault(require("../controllers/authenticationRoutes"));
const authentication = express_1.default.Router();
authentication.post("/register", authenticationRoutes_1.default.register);
authentication.post("/authenticate", authenticationRoutes_1.default.authenticate);
authentication.get("/logs", authenticationRoutes_1.default.logs);
module.exports = authentication;
//# sourceMappingURL=authentication.js.map