"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const invokeController_1 = __importDefault(require("./blockchain/invokeController"));
const register = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("chegou register auth");
    req.body.channel = "default";
    req.body.chain = "ottxcc";
    let result = yield invokeController_1.default.register(req.body);
    return res.status(result.status).json(result.data);
});
const authenticate = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    req.body.channel = "default";
    req.body.chain = "ottxcc";
    let result = yield invokeController_1.default.authenticate(req.body);
    console.log(req.body);
    return res.status(result.status).json(result.data);
});
const logs = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let data = {
        apiKey: req.headers["x-gravitee-api-key"],
        channel: "default",
        chain: "ottxcc",
    };
    let result = yield invokeController_1.default.logs(data);
    return res.status(result.status).json(result.data);
});
exports.default = {
    register,
    authenticate,
    logs,
};
//# sourceMappingURL=authenticationRoutes.js.map