declare const _default: {
    register: (data: any) => Promise<{
        status: any;
        data: {
            message: any;
        };
    }>;
    authenticate: (data: any) => Promise<{
        status: any;
        data: {
            message: any;
        };
    }>;
    logs: (data: any) => Promise<{
        status: any;
        data: {
            message: any;
        };
    } | {
        status: number;
        data: any;
    }>;
};
export default _default;
