"use strict";
/*
 * SPDX-License-Identifier: Apache-2.0
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fabric_network_1 = require("fabric-network");
const path = __importStar(require("path"));
const fs = __importStar(require("fs"));
const yaml = __importStar(require("js-yaml"));
const ccpPath = path.resolve(__dirname, "..", "..", "connection.yaml");
const ccp = yaml.load(fs.readFileSync(ccpPath, "utf8"));
const configPath = path.join(__dirname, "..", "..", "./config.json");
const configJSON = fs.readFileSync(configPath, "utf8");
const config = JSON.parse(configJSON);
const walletPath = path.join(__dirname, "..", "..", "wallet");
const register = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const wallet = yield fabric_network_1.Wallets.newFileSystemWallet(walletPath);
        const adminIdentity = yield wallet.get("admin");
        if (!adminIdentity) {
            console.log("Run the enrollAdmin.ts application before retrying");
            return {
                status: 500,
                data: {
                    message: 'An identity for the admin user "admin" does not exist in the wallet',
                },
            };
        }
        const gateway = new fabric_network_1.Gateway();
        yield gateway.connect(ccp, {
            wallet,
            identity: "admin",
            discovery: config.gatewayDiscovery,
        });
        const network = yield gateway.getNetwork(data.channel);
        const chain = network.getContract(data.chain);
        let args = JSON.stringify(data);
        let result = yield chain.submitTransaction("Invoke", "register", args);
        console.log(JSON.parse(result));
        yield gateway.disconnect();
        return {
            status: JSON.parse(result).status,
            data: {
                message: JSON.parse(result).message
                    ? JSON.parse(result).message.message
                    : "submited",
            },
        };
    }
    catch (error) {
        console.log(error);
        return { status: 500, data: { message: error.toString() } };
    }
});
const authenticate = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const wallet = yield fabric_network_1.Wallets.newFileSystemWallet(walletPath);
        const adminIdentity = yield wallet.get("admin");
        if (!adminIdentity) {
            console.log("Run the enrollAdmin.ts application before retrying");
            return {
                status: 500,
                data: {
                    message: 'An identity for the admin user "admin" does not exist in the wallet',
                },
            };
        }
        const gateway = new fabric_network_1.Gateway();
        yield gateway.connect(ccp, {
            wallet,
            identity: "admin",
            discovery: config.gatewayDiscovery,
        });
        const network = yield gateway.getNetwork(data.channel);
        const chain = network.getContract(data.chain);
        let args = JSON.stringify(data);
        let result = yield chain.submitTransaction("Invoke", "authenticate", args);
        console.log(JSON.parse(result));
        yield gateway.disconnect();
        return {
            status: JSON.parse(result).status,
            data: {
                message: JSON.parse(result).message
                    ? JSON.parse(result).message.message
                    : "submited",
            },
        };
    }
    catch (error) {
        console.log(error);
        return { status: 500, data: { message: error.toString() } };
    }
});
const logs = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const wallet = yield fabric_network_1.Wallets.newFileSystemWallet(walletPath);
        const adminIdentity = yield wallet.get("admin");
        if (!adminIdentity) {
            console.log("Run the enrollAdmin.ts application before retrying");
            return {
                status: 500,
                data: {
                    message: 'An identity for the admin user "admin" does not exist in the wallet',
                },
            };
        }
        const gateway = new fabric_network_1.Gateway();
        yield gateway.connect(ccp, {
            wallet,
            identity: "admin",
            discovery: config.gatewayDiscovery,
        });
        const network = yield gateway.getNetwork(data.channel);
        const chain = network.getContract(data.chain);
        let args = JSON.stringify(data);
        let queryResult = yield chain.evaluateTransaction("Invoke", "logs", args);
        console.log(queryResult);
        console.log("queryResult");
        yield gateway.disconnect();
        if (!JSON.parse(queryResult).payload) {
            return {
                status: JSON.parse(queryResult).status,
                data: { message: JSON.parse(queryResult).message.message },
            };
        }
        else {
            const result = JSON.parse(Buffer.from(JSON.parse(queryResult).payload).toString());
            if (result[result.length - 1].fetched_records_count < 1) {
                return {
                    status: 404,
                    data: { message: "Nothing found" },
                };
            }
            if (data.query.id) {
                return {
                    status: 200,
                    data: result[0].Record,
                };
            }
            if (result[result.length - 1].fetched_records_count > 0) {
                let newResult = {
                    data: [],
                    pagination: result[result.length - 1],
                };
                result.pop();
                newResult.data = result.map(function (item) {
                    return item.Record;
                });
                return {
                    status: 200,
                    data: newResult,
                };
            }
            return {
                status: 200,
                data: result,
            };
        }
    }
    catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        return { status: 500, data: { message: error.toString() } };
    }
});
exports.default = {
    register,
    authenticate,
    logs,
};
//# sourceMappingURL=invokeController.js.map