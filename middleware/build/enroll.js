"use strict";
/*
 * SPDX-License-Identifier: Apache-2.0
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fabric_ca_client_1 = __importDefault(require("fabric-ca-client"));
const fabric_network_1 = require("fabric-network");
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const yaml = __importStar(require("js-yaml"));
const configPath = path.join(__dirname, "./config.json");
const configJSON = fs.readFileSync(configPath, "utf8");
const config = JSON.parse(configJSON);
const connection_file = config.connection_file;
const ccpPath = path.resolve(__dirname, connection_file);
const ccp = yaml.load(fs.readFileSync(ccpPath, "utf8"));
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // Create a new CA client for interacting with the CA.
            const caInfo = ccp.certificateAuthorities[config.ca];
            const caTLSCACerts = caInfo.tlsCACerts.pem;
            const ca = new fabric_ca_client_1.default(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);
            // Create a new file system based wallet for managing identities.
            const walletPath = path.join(__dirname, "wallet");
            const wallet = yield fabric_network_1.Wallets.newFileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);
            // Check to see if we've already enrolled the admin user.
            const identity = yield wallet.get(caInfo.registrar.enrollId);
            if (identity) {
                console.log("An identity for the admin already exists in the wallet");
                return;
            }
            // Enroll the admin user, and import the new identity into the wallet.
            const enrollment = yield ca.enroll({
                enrollmentID: caInfo.registrar.enrollId,
                enrollmentSecret: caInfo.registrar.enrollSecret,
            });
            const x509Identity = {
                credentials: {
                    certificate: enrollment.certificate,
                    privateKey: enrollment.key.toBytes(),
                },
                mspId: config.mspId,
                type: "X.509",
            };
            yield wallet.put(caInfo.registrar.enrollId, x509Identity);
            console.log("Successfully enrolled admin and imported it into the wallet");
        }
        catch (error) {
            console.error(`Failed to enroll admin: ${error}`);
            process.exit(1);
        }
    });
}
main();
//# sourceMappingURL=enroll.js.map