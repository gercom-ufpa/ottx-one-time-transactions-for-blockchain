/** src/controllers/posts.ts */
import { Request, Response, NextFunction } from "express";
import invoke from "./blockchain/invokeController";

const register = async (req: Request, res: Response, next: NextFunction) => {
  console.log("chegou register auth");

  req.body.channel = "default";
  req.body.chain = "ottxcc";
  let result: any = await invoke.register(req.body);
  return res.status(result.status).json(result.data);
};
const authenticate = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.body.channel = "default";
  req.body.chain = "ottxcc";
  let result: any = await invoke.authenticate(req.body);
  console.log(req.body);

  return res.status(result.status).json(result.data);
};
const logs = async (req: Request, res: Response, next: NextFunction) => {
  let data = {
    apiKey: req.headers["x-gravitee-api-key"],
    channel: "default",
    chain: "ottxcc",
  };
  let result: any = await invoke.logs(data);
  return res.status(result.status).json(result.data);
};

export default {
  register,
  authenticate,
  logs,
};
