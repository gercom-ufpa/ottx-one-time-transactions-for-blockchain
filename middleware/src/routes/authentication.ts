/** src/routes/applications.ts */
import express from "express";
import controller from "../controllers/authenticationRoutes";
const authentication = express.Router();

authentication.post("/register", controller.register);
authentication.post("/authenticate", controller.authenticate);
authentication.get("/logs", controller.logs);

export = authentication;
