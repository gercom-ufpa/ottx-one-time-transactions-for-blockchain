/*
 * SPDX-License-Identifier: Apache-2.0
 */

import FabricCAServices from "fabric-ca-client";
import { Wallets, X509Identity } from "fabric-network";
import * as fs from "fs";
import * as path from "path";
import * as yaml from "js-yaml";

const configPath = path.join(__dirname, "./config.json");
const configJSON = fs.readFileSync(configPath, "utf8");
const config = JSON.parse(configJSON);
const connection_file = config.connection_file;
const ccpPath = path.resolve(__dirname, connection_file);
const ccp: any = yaml.load(fs.readFileSync(ccpPath, "utf8"));

async function main() {
  try {
    // Create a new CA client for interacting with the CA.
    const caInfo = ccp.certificateAuthorities[config.ca];
    const caTLSCACerts = caInfo.tlsCACerts.pem;
    const ca = new FabricCAServices(
      caInfo.url,
      { trustedRoots: caTLSCACerts, verify: false },
      caInfo.caName
    );

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(__dirname, "wallet");
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    // Check to see if we've already enrolled the admin user.
    const identity = await wallet.get(caInfo.registrar.enrollId);
    if (identity) {
      console.log("An identity for the admin already exists in the wallet");
      return;
    }

    // Enroll the admin user, and import the new identity into the wallet.
    const enrollment = await ca.enroll({
      enrollmentID: caInfo.registrar.enrollId,
      enrollmentSecret: caInfo.registrar.enrollSecret,
    });
    const x509Identity: X509Identity = {
      credentials: {
        certificate: enrollment.certificate,
        privateKey: enrollment.key.toBytes(),
      },
      mspId: config.mspId,
      type: "X.509",
    };
    await wallet.put(caInfo.registrar.enrollId, x509Identity);
    console.log("Successfully enrolled admin and imported it into the wallet");
  } catch (error) {
    console.error(`Failed to enroll admin: ${error}`);
    process.exit(1);
  }
}

main();
