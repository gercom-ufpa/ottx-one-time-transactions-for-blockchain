/*
 * SPDX-License-Identifier: Apache-2.0
 */
"use strict";
const shim = require("fabric-shim");
const ClientIdentity = require("fabric-shim").ClientIdentity;
const logger = shim.newLogger("chaincode");
const crypto = require("crypto");
const { Contract } = require("fabric-contract-api");
let userId;
class Chaincode extends Contract {
  async init() {
    logger.info("cc init", "");
    return shim.success();
  }

  async ping() {
    logger.info("ping called", "");
    const answer = { ping: "pong basic 1.15" };
    return Buffer.from(JSON.stringify(answer), "utf8");
  }

  async Invoke(ctx, txName, data) {
    const cid = new ClientIdentity(ctx.stub);
    let cidId = cid.getID();
    userId = cidId.substring(cidId.indexOf("CN=") + 3, cidId.lastIndexOf("::"));
    const method = this[txName];
    if (!method) {
      logger.info(`no function of name:${method} found`);
      return shim.error(`Received unknown function ${method} invocation`);
    }
    try {
      let args = [ctx.stub];
      args.push(data);
      args.push(this);
      let payload = await method.apply(this, args);
      console.log(payload);
      return shim.success(payload);
    } catch (err) {
      logger.info(err);
      return shim.error(err);
    }
  }

  async hash(input) {
    return crypto.createHash("sha256").update(input).digest("hex");
  }

  async verifySignedMessage(publicKey, signature, message) {
    const verifier = crypto.createVerify("RSA-SHA256");
    verifier.update(message);
    return verifier.verify(publicKey, signature, "hex");
  }

  async authenticate(stub, data, thisClass) {
    data = JSON.parse(data);
    console.info("Started authentication");

    const userDataAsBytes = await stub.getState(data.id);
    if (!userDataAsBytes.toString()) {
      throw new Error(`ID not found: ${data.id}`);
    }

    const userData = JSON.parse(userDataAsBytes.toString());
    if (userData.index !== data.index) {
      throw new Error(`Index not match: ${userData.index}`);
    }

    const hash = thisClass.hash;
    const OTPHash = await hash(data.OTP);
    if (userData.nextOTPHash !== OTPHash) {
      throw new Error("OTPHash not match");
    }

    const verifySignedMessage = thisClass.verifySignedMessage;
    const message = {
      id: data.id,
      publicKey: data.publicKey,
      nextOTPHash: userData.nextOTPHash,
      transactionData: data.transactionData,
    };

    if (
      !verifySignedMessage(
        userData.publicKey,
        data.signedMessage,
        JSON.stringify(message)
      )
    ) {
      throw new Error("Signature verification invalid");
    }

    console.info("Verified");
    userData.index++;
    userData.nextOTPHash = data.nextOTPHash;
    await stub.putState(userData.id, Buffer.from(JSON.stringify(userData)));
    await stub.putState(data.transactionData.id, Buffer.from(JSON.stringify(data.transactionData)));

    console.info("Authentication successful");
  }

  async register(stub, data, thisClass) {
    data = JSON.parse(data);
    console.info("Started registration");

    const message = {
      id: data.id,
      publicKey: data.publicKey,
      nextOTPHash: data.nextOTPHash,
    };

    const idAsBytes = await stub.getState(data.id);
    if (idAsBytes.toString()) {
      throw new Error("Already registered");
    }

    const verifySignedMessage = thisClass.verifySignedMessage;
    if (
      !verifySignedMessage(
        data.publicKey,
        data.signedMessage,
        JSON.stringify(message)
      )
    ) {
      throw new Error("Signature verification invalid");
    }

    await stub.putState(
      data.id,
      Buffer.from(
        JSON.stringify({
          id: data.id,
          publicKey: data.publicKey,
          nextOTPHash: data.nextOTPHash,
          index: 1,
        })
      )
    );

    console.info("Registration successful");
  }
}

module.exports = Chaincode;
