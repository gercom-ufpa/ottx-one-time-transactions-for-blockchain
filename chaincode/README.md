## First Install
sudo minifab cleanup && sudo rm -rf vars
sudo minifab up -o org1.example.com -e 7050 -s couchdb -n samplecc -p ''
sudo minifab discover
minifab create -c default

minifab join

copy cc to vars/chaincode/[ccname]/[cclang]/

minifab install -n ottxcc -l node -v 1.0.0 && minifab approve,commit,discover && minifab initialize
minifab profilegen
copy node profile yaml
enroll admin (yarn enroll)

## Update
minifab install -n ottxcc -v 1.0.3 && minifab approve,commit,discover && minifab initialize

