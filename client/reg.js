const crypto = require("crypto");
const axios = require("axios");
let media = 0;
console.log(crypto);
// Authentication process
let x;
let y;
let message;
let delta;
// Hash function H

function hash(input) {
  return crypto.createHash("sha256").update(input).digest("hex");
}

// Signature algorithm Ssk
function sign(privateKey, message) {
  const signer = crypto.createSign("RSA-SHA256");
  signer.update(message);
  return signer.sign(privateKey, "hex");
}

// Key generation algorithm G
function generateKeys() {
  const { privateKey, publicKey } = crypto.generateKeyPairSync("rsa", {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: "spki",
      format: "pem",
    },
    privateKeyEncoding: {
      type: "pkcs8",
      format: "pem",
    },
  });

  return { privateKey, publicKey };
}

async function register(id, pw) {
  let i = 0;
  
  const start = Date.now();
  //client
  const { privateKey, publicKey } = generateKeys();
  x = hash(JSON.stringify({ id, i, pw, sk: privateKey }));
  y = hash(x);
  message = { id, pk: publicKey, y };
  delta = sign(privateKey, message.toString());
  let data = {
    id,
    pk: publicKey,
    y,
    delta,
  };

  //axios blockchain register data
  await axios
    .post("http://localhost:3001/register", data)
    .then(function (response) {
      x = hash(JSON.stringify({ id, i, pw, sk: privateKey }));
      i++;
      y = hash(hash(JSON.stringify({ id, i, pw, sk: privateKey })));
      message = { id, pk: publicKey, y };
      delta = sign(privateKey, message.toString());
      let newotp = {
        id,
        pw,
        otp: { id, x, y, i, delta },
        sk: privateKey.toString(),
        pk: publicKey.toString(),
      };
      console.log("newotp");
      console.log(newotp);
      // const end = Date.now();
      // console.log(`Execution time: ${end - start} `);
      const end = Date.now();
      console.log(`Execution time: ${end - start} `);
      media = media + end - start;
      console.log(`Media Execution time: ${media / 10} `);
      console.log(`Total Execution time: ${media} `);
      console.log(response.data);
      console.log("ok");
    })
    .catch(function (error) {
      console.log(error.response.data);
      console.log("error");
    });
}

// const input1 = process.argv[2];
// const input2 = process.argv[3];
for (let index = 0; index < 1; index++) {
  setTimeout(() => {
    register(Date.now().toString(), "pw");
  }, 0);
}
