/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly VITE_WALLET_SERVER_HOST: string;
  readonly VITE_WALLET_SERVER_PORT: number;
  // more env variables...
}
interface ImportMeta {
  readonly env: ImportMetaEnv;
}
