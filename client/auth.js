const crypto = require("crypto");
const axios = require("axios");
let media = 0;

// Authentication process
let x;
let y;
let message;
let delta;
// Hash function H
function hash(input) {
  return crypto.createHash("sha256").update(input).digest("hex");
}

// Signature algorithm Ssk
function sign(privateKey, message) {
  const signer = crypto.createSign("RSA-SHA256");
  signer.update(message);
  return signer.sign(privateKey, "hex");
}
let txs = 3;
let txsv = 0;
let user = {
  id: '1676532564814',
  pw: 'pw',
  otp: {
    id: '1676532564814',
    x: 'e3e1a5321c625e05e1827bf564a351efeebf2555ec8bd75338610dff76bb0a05',
    y: 'be359f507bf686260606d017d486caa76bce6301a798dec00284eba0c7086027',
    i: 3,
    delta: '6f9273fdfdc9dd01da85f7dc23e78c710637c5379b08e214cf8de13e56e97291e56d1315a8ef912ee4066e9978bca5b5098fd1e20b583dbe63ce7a87a2227fef37d86c1442c8aa238c8ebd4eb8610cee12293533292d76e7f4e251f8279949f4c381a6d55e220071400a24241f6af22c31575c3b2eac63ee88274f782be33b91ba744a7827319a4c0abbdcae69e86a90f3c933032679b8a008e172d9f2bb164aeb26f07ed1d66146465b28f9eaf53f881f753c7827d557977f5934c4d4c4e86bbacd7341589986fa8d7036396bb484c18e00e1498771bce30d4886e6a8fcb7762e3758add2156f132c106aabcf6bac4a1ced0dd07ce9735e2f2031b93f0c91e0'
  },
  sk: '-----BEGIN PRIVATE KEY-----\n' +
    'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDBVyFx88CUdU3G\n' +
    'ZsHcwezWuOwSPlyrFceUco7wqQYvDygiRJN+zaHru1jEegTCeye7i/84ZWXqDfV3\n' +
    'B38xcMY30GcjAsVSzq2/tiGSLKAZ0er27FXMkUDYI9+R6oxHoOphorSUiipgAN1T\n' +
    'omXVJs0j7RjFBZPmtKaz6gNZwwZVev6hKXLZiH+ahHfKX2iqWDmoaxa2rpd5GKTD\n' +
    'M/IBD3PSxJ6MqEQg5yYyW25NZ5RT7wlwc2VpK8HGCT096sJP5ek+dP34R8rn7246\n' +
    'Ca1Hgp+nCmlFWZBvYZB8Slr2F1eFQR19ZPFiCexYEYatNmmeTgnI3kwe6h5cCjQ4\n' +
    'r43/Pa0DAgMBAAECggEAIHK2G0DUhlX1mqGuUZV+YT8h56HtvYznNQPQ95T2g7lg\n' +
    'e/fUZUivrzd/G2J7IAS5U2GYkOd7pj93feWVNEAAFGf2l0qkwVQMlwtDMRwqNosj\n' +
    'ixN5Bt8t6diDciLSLkdUbtR2yCM0H26ltp6fxpk/fpMRQCfCfcq9254ft9XiXz/9\n' +
    'gV/sQlR9pNIEo4i9AKuiKxdAnPdaalFB4H54zzbl/DgVLvdbmDyuv7FxzMt+bobk\n' +
    'CjVn7ODL7Hj2P9MUJH1cHesxUQP6MWnjGTKZJsyYc7Y6mw2Cbp3XdzVGoqk6ysrU\n' +
    'PHKyAirzD6lE9+x2JVDPr3X5YYqIgPhyxTgoUWvysQKBgQDaO0WhyXaj2m3za4NR\n' +
    'SOtNVnbzufPTf4eLXEui2CEtFz5abpoSYLaLInaZ019NNRybWtVfObs6MH9P6Rua\n' +
    '6i1BcFhadFq1r++SHo0+0jvoPRc3HNKgoE1BSuGZu71I77W/RVQ8hVp53qPZ/z6n\n' +
    'lqB9pNkIbelL4k8yIY3skW1myQKBgQDizQ6sldAVlMIQtHqmfPHfbqo+9WAf2S0D\n' +
    'S/JzMlcx1DeuS80a1Z6aWud4yqBTvCEQAVWi42JESlEwgeiAP1pJAA1vThWlZbWZ\n' +
    '43KCHbjfrBTMI1t+ntL/i5rwYl1IGIVQ4EYuc/kJcauSKj7Wcny3ftLa04WUsT37\n' +
    'UzqLoVZ/awKBgQDPtl6+cOz1ZDAG4qwosAkWoVey/6juA5JVuNFc++4H5vm4RVFs\n' +
    'cJUOtQzBuQgaipTh4YD5lA96gTKMMsnNl1rf0DioT9sO15hlMaMqyhuvHoZ8NHjd\n' +
    '3fXqUYWNN74gL9iDZ0+YEy1clebK1UEjbiDvtfnFJihJy+upQXq+UmUY0QKBgC32\n' +
    'AjBFV9rXv5AlEunkThvKUGPb0/QATdfqLyLbI8JsUDBZGiKDAdYj796RqXXunQCi\n' +
    'VJli3QobyWI8IULEE+bRROe2ejp/ZxLdfdoSbeGd/MT1sVfyla3Fu8tVE7RFi9a8\n' +
    '7FwDkocwauV+/Cb3IoEJCW/hB6P/WyEwV5/uPRorAoGAPJyQE87z3ZSzZ/ip+Wr2\n' +
    'TxIuCz/XSPj6JeMDDQix+jQwbz924EZz5QbSbiRGwxaVBngm8f+eK/jLakUj2W9i\n' +
    'RWcfOGBPM3Y0LkX7FgByx3t9opSR/Wggf3AeEV29JuN+9gDVVceI1V1zUohzyQUS\n' +
    'KsJB75UeqYd79mV5KEFHNLg=\n' +
    '-----END PRIVATE KEY-----\n',
  pk: '-----BEGIN PUBLIC KEY-----\n' +
    'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwVchcfPAlHVNxmbB3MHs\n' +
    '1rjsEj5cqxXHlHKO8KkGLw8oIkSTfs2h67tYxHoEwnsnu4v/OGVl6g31dwd/MXDG\n' +
    'N9BnIwLFUs6tv7YhkiygGdHq9uxVzJFA2CPfkeqMR6DqYaK0lIoqYADdU6Jl1SbN\n' +
    'I+0YxQWT5rSms+oDWcMGVXr+oSly2Yh/moR3yl9oqlg5qGsWtq6XeRikwzPyAQ9z\n' +
    '0sSejKhEIOcmMltuTWeUU+8JcHNlaSvBxgk9PerCT+XpPnT9+EfK5+9uOgmtR4Kf\n' +
    'pwppRVmQb2GQfEpa9hdXhUEdfWTxYgnsWBGGrTZpnk4JyN5MHuoeXAo0OK+N/z2t\n' +
    'AwIDAQAB\n' +
    '-----END PUBLIC KEY-----\n'
};
async function authentication(otp, id, pw, sk, pk) {
  const start = Date.now();
  //axios blockchain authentication
  await axios
    .post("http://localhost:3001/authenticate", otp)
    .then(function (response) {
      x = hash(JSON.stringify({ id, i: otp.i, pw, sk: sk }));
      otp.i++;
      y = hash(JSON.stringify({ id, i: otp.i, pw, sk: sk }));
      message = { id, pk: pk, y };
      delta = sign(sk, message.toString());
      let newData = {
        id,
        pw,
        otp: { id, x, y, i: otp.i, delta },
        sk: sk,
        pk: pk
      };
      console.log("newData");
      console.log(newData);

      const end = Date.now();
      console.log(`Execution time: ${end - start} `);
      media = media + end - start;
      console.log(`Media Execution time: ${media / txs} `);
      console.log(`Total Execution time: ${media} `);
      user = newData;
      if (txsv < txs) {
        authentication(user.otp, user.id, user.pw, user.sk, user.pk);
      }
      txsv++;
      console.log(response.data);
      return;
    })
    .catch(function (error) {
      console.log(error.response.data);
      console.log("error");
    });
}
authentication(user.otp, user.id, user.pw, user.sk, user.pk);
