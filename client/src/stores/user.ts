import { defineStore } from 'pinia';

export const useUserStore = defineStore({
  id: 'userStore',
  state: () => ({
    data: {
      "id": "",
      "pw": "",
      "userData": { privateKey: '', publicKey: ''},
      "otp": {
          "id": "",
          "x": "",
          "y": "",
          "i": 1,
          "delta": ""
      },
      "sk": "",
      "pk": ""
  }
  })
});