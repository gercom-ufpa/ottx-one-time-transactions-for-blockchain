function generateToken(
  id,
  password,
  privateKey,
  publicKey,
  index,
  txData,
  appId,
  serviceId
) {
  if (index === 0) {
    let token = hash({ id, index, password, privateKey });
    let nextToken = hash(token);
    let message = { id, publicKey, nextToken };
    let signedMessage = sign(privateKey, message);
    let result = { id, token, nextToken, index, signedMessage };
    return result;
  } else {
    let newToken = hash({ id, index, password, privateKey });
    let nextToken = hash(newToken);
    let message = { id, publicKey, nextToken, txData, appId, serviceId };
    let signedMessage = sign(privateKey, message);
    let previousToken = hash({ id, index: index - 1, password, privateKey });
    let result = { id, previousToken, nextToken, index, signedMessage };
    return result;
  }
}

function authenticate(
  index,
  id,
  publicKey,
  token,
  nextToken,
  txData,
  appId,
  signedMessage
) {
  const serviceId = ClientIdentity().getID()
  const userData = getState(id);
  if (!userData) {
    throw new Error(`ID not found: ${id}`);
  }
  if (userData.index !== index) {
    throw new Error(`Index not match: ${userData.index}`);
  }
  const nextToken = hash(token);
  if (userData.nextToken !== nextToken) {
    throw new Error("Next Token not match");
  }
  const message = {
    id,
    publicKey,
    nextToken: userData.nextToken,
    txData,
    appId,
    serviceId,
  };
  if (!verifySignedMessage(userData.publicKey, signedMessage, message)) {
    throw new Error("Signature verification invalid");
  }
  userData.index++;
  userData.nextToken = nextToken;
  putState(userData.id, userData);
  putState(txData.id, txData);
}

function register(id, publicKey, nextToken, signedMessage) {
  const message = {
    id: id,
    publicKey,
    nextToken,
  };
  const userData = getState(id);
  if (userData) {
    throw new Error("Already registered");
  }
  if (!verifySignedMessage(publicKey, signedMessage, message)) {
    throw new Error("Signature verification invalid");
  }
  putState({
    id: id,
    publicKey: publicKey,
    nextToken: nextToken,
    index: 1,
  });
}
