import Api from "@/services/api";
export default {
  async postRegister(data: any) {
    return Api().post("/register", data);
  },
  postAuthenticate(data: any) {
    return Api().post("/authenticate", data);
  },
};
