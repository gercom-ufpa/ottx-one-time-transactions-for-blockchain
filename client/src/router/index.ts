import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/authenticate",
      name: "authenticate",
      // route level code-splitting
      // this generates a separate chunk (Authenticate.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AuthenticateView.vue"),
    },
    {
      path: "/enroll",
      name: "enroll",
      // route level code-splitting
      // this generates a separate chunk (Enroll.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/EnrollView.vue"),
    },
  ],
});

export default router;
